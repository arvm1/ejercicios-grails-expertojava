/* Añade aquí la implementación solicitada en el ejercicio */
// anyadir un método llamado moneda que recibirá por parámetro un locale ( es_ES, en_EN o en_US)

def localeEN = "en_EN"
def localeUS = "en_US"
def localeES = "es_ES"

def EN = "£"
def US = "\$"
def ES = "€"

Number.metaClass.moneda = { locale ->
    if(locale == localeEN) "$EN$delegate"
    else if(locale == localeUS) "$US$delegate"
    else if(locale == localeES) "$delegate$ES"
}


assert new Float(10.2).moneda("en_EN") ==  "£10.2"
assert new Float(10.2).moneda("en_US") ==  "\$10.2"
assert new Float(10.2).moneda("es_ES") ==  "10.2€"

assert 10.2.moneda("en_EN") ==  "£10.2"
assert 10.2.moneda("en_US") ==  "\$10.2"
assert 10.2.moneda("es_ES") ==  "10.2€"

assert 10.moneda("en_EN") ==  "£10"
assert 10.moneda("en_US") ==  "\$10"
assert 10.moneda("es_ES") ==  "10€"

assert new Double(10.2).moneda("en_EN") ==  "£10.2"
assert new Double(10.2).moneda("en_US") ==  "\$10.2"
assert new Double(10.2).moneda("es_ES") ==  "10.2€"