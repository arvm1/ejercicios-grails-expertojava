/* Añade aquí la implementación del factorial en un closure */

def factorial = {numero ->
    numFactorial = numero
    for(i in 1..numero-1) 
        numFactorial *=i
    return numero==1 ? 1:numFactorial
}

assert factorial(1)==1
assert factorial(2)==1*2
assert factorial(3)==1*2*3
assert factorial(4)==1*2*3*4
assert factorial(5)==1*2*3*4*5
assert factorial(6)==1*2*3*4*5*6
assert factorial(7)==1*2*3*4*5*6*7
assert factorial(8)==1*2*3*4*5*6*7*8
assert factorial(9)==1*2*3*4*5*6*7*8*9
assert factorial(10)==1*2*3*4*5*6*7*8*9*10

/* Añade a partir de aquí el resto de tareas solicitadas en el ejercicios */

// Recorremos una Lista de enteros, calculamos el factorial de cada uno y lo mostramos por pantalla.

miListaDeEnteros = [2,2,3,3,4,4,5,6]
assert miListaDeEnteros instanceof ArrayList

println "\nRecorriendo varias veces la misma lista"
for (i in miListaDeEnteros){
    println factorial(i)
}

println "\n"
for (i in 1.. miListaDeEnteros.size()){
    println factorial(miListaDeEnteros[i-1])
}

println "\n"
miListaDeEnteros.each{
    println factorial(it)
}

// Recorremos una mapa de enteros, calculamos el factorial de cada uno y lo mostramos por pantalla.

miMapa = [1:2,2:2,3:3,4:3,5:4,6:4,7:5,8:6]

println "\nRecorriendo un mapa"
for(value in miMapa.values()){
    println factorial(value)
}


// Closure ayer y mañana
Date fechaParametro = new Date().parse("d/M/yyyy H:m:s","7/2/2016 00:30:20")

def ayer = { fecha ->
    Date ayer = fecha-1
}

def manyana = { fecha ->
    Date tomorrow = fecha+1
}

assert ayer(fechaParametro)    == new Date().parse("d/M/yyyy H:m:s","6/2/2016 00:30:20")
assert manyana(fechaParametro) == new Date().parse("d/M/yyyy H:m:s","8/2/2016 00:30:20")

UnoDeEnero =     new Date().parse("d/M/yyyy H:m:s","1/1/2016 00:00:00")
DosDeFebrero = new Date().parse("d/M/yyyy H:m:s","1/2/2016 00:00:00")
TresDeMarzo =     new Date().parse("d/M/yyyy H:m:s","1/3/2016 00:00:00")

def fechas = []
fechas.add(UnoDeEnero)
fechas.add(DosDeFebrero)
fechas.add(TresDeMarzo)

fechas.each{
    println "Día de la lista: $it"
    println "Día anterior: ${ayer(it)}"
    println "Día anterior: ${manyana(it)}\n***    ***\n"
}

for (i in 1..fechas.size()){
    println "Día de la lista: ${fechas[i-1]}"
    println "Día anterior: ${ayer(fechas[i-1])}"
    println "Día anterior: ${manyana(fechas[i-1])}\n***    ***\n"
}