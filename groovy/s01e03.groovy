

def cadena = """En un lugar de la Mancha, de cuyo nombre no quiero acordarme, no ha mucho tiempo que vivía un 
hidalgo de los de lanza en astillero, adarga antigua, rocín flaco y galgo corredor. Una olla de algo más vaca 
que carnero, salpicón las más noches, duelos y quebrantos los sábados, lentejas los viernes, algún palomino de añadidura 
los domingos, consumían las tres partes de su hacienda. El resto della concluían sayo de velarte, calzas de velludo para las 
fiestas con sus pantuflos de lo mismo, los días de entre semana se honraba con su vellori de lo más fino. Tenía en su casa 
una ama que pasaba de los cuarenta, y una sobrina que no llegaba a los veinte, y un mozo de campo y plaza, que así ensillaba 
el rocín como tomaba la podadera. Frisaba la edad de nuestro hidalgo con los cincuenta años, era de complexión recia, seco de 
carnes, enjuto de rostro; gran madrugador y amigo de la caza. Quieren decir que tenía el sobrenombre de Quijada o Quesada 
(que en esto hay alguna diferencia en los autores que deste caso escriben), aunque por conjeturas verosímiles se deja entender 
que se llama Quijana; pero esto importa poco a nuestro cuento; basta que en la narración dél no se salga un punto de la verdad."""

/* Añade aquí la implementación solicitada */

patronDeBusqueda = ~/\b\S+\b/
def palabras=[]

cadena.eachMatch(patronDeBusqueda) { match ->
    
    // Creará un nuevo array si anteriormente no existe 
    if( palabras[match.size()] == null){
        palabras[match.size()] = new ArrayList() // palabras[match.size()]=[]
    }
    
    lowercase = match.toLowerCase()
    
    /* Añadirá al Array que corresponda la palabra si esta no está previamente dentro.
       find() --> se le pasa una palabra por parámetro y "finds the first item matching the IDENTITY Closure (por ejemplo la verdad de Groovy) */
    if ( palabras[match.size()].find{it==lowercase} == null){
        palabras[match.size()].add(lowercase)
    }
 } 

assert palabras[1] == ['y','a','o']
assert palabras[2] == ['en','un','de','la','no','ha','su','el','lo','se']
assert palabras[3] == ['que','los','una','más','las','con','sus','ama','así','era','hay','por','dél']
assert palabras[4] == ['cuyo','olla','algo','vaca','tres','sayo','para','días','fino','casa','mozo','como','edad','años','seco','gran','caza','esto','caso','deja','pero','poco']
assert palabras[5] == ['lugar','mucho','vivía','lanza','rocín','flaco','galgo','algún','resto','della','mismo','entre','tenía','campo','plaza','recia','amigo','decir','deste','llama','basta','salga','punto']
assert palabras[6] == ['mancha','nombre','quiero','tiempo','adarga','noches','duelos','partes','calzas','semana','pasaba','veinte','tomaba','carnes','enjuto','rostro','alguna','aunque','cuento','verdad']
assert palabras[7] == ['hidalgo','antigua','carnero','sábados','viernes','velarte','velludo','fiestas','honraba','vellori','sobrina','llegaba','frisaba','nuestro','quieren','quijada','quesada','autores','quijana','importa']
assert palabras[8] == ['corredor','salpicón','lentejas','palomino','domingos','hacienda','cuarenta','podadera','escriben','entender']
assert palabras[9] == ['acordarme','astillero','añadidura','consumían','concluían','pantuflos','ensillaba','cincuenta','narración']
assert palabras[10] == ['quebrantos','complexión','madrugador','diferencia','conjeturas']
assert palabras[11] == ['sobrenombre', 'verosímiles']