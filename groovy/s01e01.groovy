public class Todo{

    String titulo
    String descripcion
    
    public Todo(){}
    
    public Todo(String tit,String des){
        titulo = tit
        descripcion = des
    }
}

def todos = []
        todos.add(new Todo("Lavadora","Poner lavadora"));
        todos.add(new Todo("Impresora","Comprar cartuchos impresora"));
        todos.add(new Todo("Películas","Devolver películas videoclub"));

todos.each{ 
    println "${it.getTitulo()} - ${it.getDescripcion()}"
} 

// puedo poner un return al final para que no me devuelva nada , si no lo hago me devuelve la última operación que haga.