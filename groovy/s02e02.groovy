class Calculadora {

    static int op1,op2,option
    static String mensaje
    
    static suma = { x,y ->   
        return x + y
    }
    
    static resta = { x,y ->
        return x - y
    }
    
    static multiplica = { x,y ->
        return x * y
    }
    
    static divide = { x,y ->
        return x/y
    }
    
    static dameEntero = { mensaje ->
        def isValid = false
        int numero
        while (!isValid){
            try{
                def entrada = System.console().readLine mensaje
                if((numero = Integer.parseInt(entrada.trim())) instanceof Integer ){
                    isValid = true
                }
            }catch(NumberFormatException e){
                mensaje = "Por favor introduce un entero válido: "
            }
        }
        return numero
    }
    
    static int imprimeOpciones(){
        boolean isValid = false
        boolean isFirstTime = true
        int opcion
        mensaje =  """\nElige una de las siguientes opciones: 1,2,3,4 o 5:          
        1 .- Sumar
        2 .- Restar
        3 .- Multiplicar
        4 .- Dividir
        5 .- Salir\n
        Escoge tu opción: """
       
        while (!isValid){
            opcion = dameEntero(mensaje)
            if(opcion !=1 && opcion !=2 && opcion !=3 && opcion !=4 && opcion !=5)
                mensaje = "Por favor escoge una opcion valida: "
            else
                isValid = true;
        }   
        return opcion 
    }
    
    static ejecutaOpcion = {opcion ->
        String mensajeSalida = "Resultado de la operación: "  
        String result = ''
        
        switch(opcion){
            case 1:
                result = Calculadora.suma(op1,op2)
                break
            
            case 2:
                result = Calculadora.resta(op1,op2)
                break
                
            case 3:
                result = Calculadora.multiplica(op1,op2)
                break
                
            case 4:
                try{
                    result = Calculadora.divide(op1,op2)
                }catch(ArithmeticException ae){
                    mensajeSalida = "No se puede dividir por 0, inténtalo de nuevo!!"
                }    
                break
                
            case 5:
                return
        } 
           
        println mensajeSalida + result
    }
    
    static arrancaCalculadora(){
        while (option!=5){
        option = imprimeOpciones()
            if(option!=5){
                op1 = dameEntero("Introduce operando 1: ")
                op2 = dameEntero("Introduce operando 2: ")
            }
            ejecutaOpcion(option)
        }        
    }
    
    static main(args){
        Calculadora.arrancaCalculadora()
    }   
}