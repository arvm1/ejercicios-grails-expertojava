package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Tag)
class TagSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    def "El nombre de la etiqueta no puede ser null"() {
        given:
        def etiqueta = new Tag(name:null)
        when:
        etiqueta.validate()
        then:
        etiqueta?.errors['name']
    }
}
