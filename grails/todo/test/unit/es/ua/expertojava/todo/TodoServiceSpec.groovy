package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(TodoService)
class TodoServiceSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    // Ponemos este método para que no falle el test de la clase
    def "El método plus de los números funciona correctamente"  () {
        when:
        def result = 10.plus(2)
        then:
        result == 12
    }


}
