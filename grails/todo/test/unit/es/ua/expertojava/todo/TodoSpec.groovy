package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Todo)
class TodoSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    @Unroll
    def " Si la reminderdate #reminderDate es posterior a la date #date, no dará problemas"() {
        given:
            def todo1 = new Todo()
            todo1.date= date
            todo1.reminderDate=reminderDate
        when:
            todo1.validate()
        then:
            !todo1?.errors['reminderDate']
        where:
            date             |   reminderDate
            new Date()       |   new Date()-1

    }
    @Unroll
    def " La reminderdate #reminderDate no puede ser posterior a la date #date"() {
        given:
            def todo1 = new Todo()
            todo1.date= date
            todo1.reminderDate=reminderDate
        when:
            todo1.validate()
        then:
            todo1?.errors['reminderDate']
        where:
            date             |   reminderDate
            new Date()       |   new Date()
            new Date()       |   new Date() + 1

    }

    void "test something"() {
    }
}
