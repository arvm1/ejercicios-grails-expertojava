package es.ua.expertojava.todo

import grails.transaction.Transactional
import groovy.time.TimeCategory


class TodoService {
    def springSecurityService

    def serviceMethod() {

    }

    def showUserTodos2(User user){
        Todo.findAllByUser(user)
    }

    def showUserTodosByCategorias(ArrayList arrayIdsCategorias,User user){
        ArrayList categorias = new ArrayList();

        arrayIdsCategorias.each{id ->
            Category category = Category.get(id)
            categorias.add(category)
        }
        Todo.findAllByUserAndCategoryInList(user,categorias,["sort":"date","order":"desc"])
    }

    def saveTodo(Todo todoInstance){

        if(todoInstance.realizada){
            todoInstance.dateDone = new Date()
        }else{
            todoInstance.dateDone = null
        }
        todoInstance.save flush:true
    }

    def lastTodosDone(Integer horas){
        def fechaPasado
        use(TimeCategory){
            fechaPasado = (-horas).hours.from.now
        }
        Todo.findAllByDateDoneBetween(fechaPasado,new Date())
    }

    def delete(todoInstance){
        def tags = new ArrayList()

        todoInstance.tags.each { tag ->
            tags.add(tag)
        }

        tags.each{tag->
            todoInstance.removeFromTags(tag)
        }
        todoInstance.delete flush:true
    }
}
