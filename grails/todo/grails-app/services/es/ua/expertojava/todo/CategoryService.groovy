package es.ua.expertojava.todo

import grails.transaction.Transactional


class CategoryService {

    def serviceMethod() {

    }

    def delete(Category categoryInstance){

        categoryInstance.todos.each{todo ->
            todo.setCategory(null)
        }
        categoryInstance.delete flush:true
    }

}
