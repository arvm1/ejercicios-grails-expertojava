

import es.ua.expertojava.todo.*

class BootStrap {


    def init = { servletContext ->
        try {

            // Seguridad
            def roleAdministrador = new Role(authority:"ROLE_ADMIN").save()
            def roleUsuario = new Role(authority:"ROLE_BASIC").save()

            def userAdmin = new User(username: "usuario",password: "admin",name:"ana",surnames: "vicente moreno",confirmPassword: "",email: "ana@ana.es" ).save()
            def userUsuario1 = new User(username: "usuario1",password: "usuario1",name:"marco",surnames: "albertsma",confirmPassword: "",email: "marco@ana.es" ).save()
            def userUsuario2 = new User(username: "usuario2",password: "usuario2",name:"pablo",surnames: "albertsma",confirmPassword: "",email: "pablo@ana.es" ).save()

//            new PersonRole(role: roleAdministrador,person: userAdmin).save() // PersoneRole.create(userAdmin,roleAdministrador)
//            new PersonRole(role: roleUsuario,person: userUsuario1).save()
//            new PersonRole(role: roleUsuario,person: userUsuario2).save()

            PersonRole.create(userAdmin,roleAdministrador)
            PersonRole.create(userUsuario1,roleUsuario)
            PersonRole.create(userUsuario2,roleUsuario)

            def categoryHome = new Category(name: "Hogar").save()
            def categoryJob = new Category(name: "Trabajo").save()

            def tagEasy = new Tag(name: "Fácil").save()
            def tagDifficult = new Tag(name: "Difícil").save()
            def tagArt = new Tag(name: "Arte").save()
            def tagRoutine = new Tag(name: "Rutina").save()
            def tagKitchen = new Tag(name: "Cocina").save()

            def todoPaintKitchen = new Todo(title: "Pintar cocina", date: new Date() + 1, realizada: "true", user: userUsuario1)
            def todoCollectPost = new Todo(title: "Recoger correo postal", date: new Date() + 2, realizada: "false", user: userUsuario1)
            def todoBakeCake = new Todo(title: "Cocinar pastel", date: new Date() + 4, realizada: "true", user: userUsuario1)
            def todoWriteUnitTests = new Todo(title: "Escribir tests unitarios", date: new Date(), realizada: "false", user:userUsuario2)

            todoPaintKitchen.addToTags(tagDifficult)
            todoPaintKitchen.addToTags(tagArt)
            todoPaintKitchen.addToTags(tagKitchen)
            todoPaintKitchen.category = categoryHome
            todoPaintKitchen.save()

            todoCollectPost.addToTags(tagRoutine)
            todoCollectPost.category = categoryJob
            todoCollectPost.save()

            todoBakeCake.addToTags(tagEasy)
            todoBakeCake.addToTags(tagKitchen)
            todoBakeCake.category = categoryHome
            todoBakeCake.save()

            todoWriteUnitTests.addToTags(tagEasy)
            todoWriteUnitTests.category = categoryJob
            todoWriteUnitTests.save()




        }catch(Exception e){
            System.out.println("Error dentro de BootStrap: " + e.printStackTrace());
        }
    }
    def destroy = { }
}

