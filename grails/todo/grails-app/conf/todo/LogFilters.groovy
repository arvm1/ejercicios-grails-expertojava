package todo

class LogFilters {
    def springSecurityService

    def filters = {
        all(controller:'todo|category|tag|user', action:'*') {
            before = {

            }
            after = { Map model ->
                log.trace("User ${(springSecurityService.getCurrentUser())} -- Controlador ${(controllerName)} -- Método ${actionName} -- Modelo ${model}")
            }
            afterView = { Exception e ->

            }
        }
    }
}
