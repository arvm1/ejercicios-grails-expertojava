<%@ page import="es.ua.expertojava.todo.Tag" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'tag.label', default: 'Tag')}" />
    <title><g:message code="default.delete.label" args="[entityName]" /></title>
    <style type="text/css" media="screen">
        #warningImage {
            width: 130px;
            height: 130px;
            margin-left: 10px;
            margin-right: 10px;
        }
        #mensajeConfirmacion{
            font-size: 2em;
        }
    </style>
</head>
<body>
<a href="#delete-tag" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
    </ul>
</div>
<div id="delete-tag" class="content scaffold-edit" role="main">
    <h1><g:message code="default.delete.label" args="[entityName]"/></h1>

    <g:hasErrors bean="${tagInstance}">
        <ul class="errors" role="alert">
            <g:eachError bean="${tagInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>

</div class="nav" role="navigation">
    <g:form url="[resource:tagInstance, action:'delete']" method="DELETE" >
        <g:hiddenField name="version" value="${tagInstance?.version}" />
        <asset:image id="warningImage" src="warning.jpg" alt="mensajeAviso"/>
        <span id="mensajeConfirmacion"><g:message code="default.button.delete.confirm.message"/></span>
        <fieldset class="buttons">
            <g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" />
        </fieldset>
    </g:form>
</div>

</body>
</html>
