<%@ page import="es.ua.expertojava.todo.Tag" %>



<div class="fieldcontain ${hasErrors(bean: tagInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="tag.name.label" default="Name" />
		
	</label>
	<g:textField name="name" value="${tagInstance?.name}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: tagInstance, field: 'color', 'error')} ">
	<label for="color">
		<g:message code="tag.color.label" default="Color" />
		
	</label>
	<g:textField name="color" value="${tagInstance?.color}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: tagInstance, field: 'todos', 'error')} ">
	<label for="todos">
		<g:message code="tag.todos.label" default="Todos" />
		
	</label>
	<g:select name="todos" from="${es.ua.expertojava.todo.Todo.list()}" multiple="multiple" optionKey="id" size="5" value="${tagInstance?.todos*.id}" class="many-to-many"/>

</div>

