<div id="header">
    <div id="menu">
        <nobr>
%{--meter el logout en un form e indicarle que es method=POST--}%
            <sec:ifLoggedIn>
                <b>Bienvenido <sec:username/>!!!</b>
                <form name="submitForm" method="POST" action="${createLink(controller: 'logout')}">
                    <input type="hidden" name="" value="">
                    %{--Función de js que simula el submit de un formulario poniendo en marcha el action del form.--}%
                    <a HREF="javascript:document.submitForm.submit()">Logout</a>
                </form>
            </sec:ifLoggedIn>
            <sec:ifNotLoggedIn>
                <g:link controller="login" action="auth">Login</g:link>
            </sec:ifNotLoggedIn>
        </nobr>
    </div>
</div>

