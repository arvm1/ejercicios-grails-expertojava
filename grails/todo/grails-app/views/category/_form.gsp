<%@ page import="es.ua.expertojava.todo.Category" %>



<div class="fieldcontain ${hasErrors(bean: categoryInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="category.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${categoryInstance?.name}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: categoryInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="category.description.label" default="Description" />
		
	</label>
	<g:textArea name="description" cols="40" rows="5" maxlength="1000" value="${categoryInstance?.description}"/>

</div>
<!-- Ejericicio 3.6 GRAILS Eliminamos enlace para crear tareas-->
%{--<div class="fieldcontain ${hasErrors(bean: categoryInstance, field: 'todos', 'error')} ">--}%
	%{--<label for="todos">--}%
		%{--<g:message code="category.todos.label" default="Todos" />--}%
		%{----}%
	%{--</label>--}%
	%{----}%
%{--<ul class="one-to-many">--}%
%{--<g:each in="${categoryInstance?.todos?}" var="t">--}%
    %{--<li><g:link controller="todo" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></li>--}%
%{--</g:each>--}%
%{--<li class="add">--}%
%{--<g:link controller="todo" action="create" params="['category.id': categoryInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'todo.label', default: 'Todo')])}</g:link>--}%
%{--</li>--}%
%{--</ul>--}%


%{--</div>--}%

