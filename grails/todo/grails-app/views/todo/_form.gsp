<%@ page import="es.ua.expertojava.todo.Todo" %>



<div class="fieldcontain ${hasErrors(bean: todoInstance, field: 'title', 'error')} required">
	<label for="title">
		<g:message code="todo.title.label" default="Title" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="title" required="" value="${todoInstance?.title}"/>

</div>

%{--<g:hiddenField id="user" name="user" value="${userInstance}" />--}%

<div class="fieldcontain ${hasErrors(bean: todoInstance, field: 'realizada', 'error')} ">
	<label for="realizada">
		<g:message code="todo.realizada.label" default="Realizada" />
		
	</label>
	<g:checkBox name="realizada" value="${todoInstance?.realizada}" />

</div>

<div class="fieldcontain ${hasErrors(bean: todoInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="todo.description.label" default="Description" />
		
	</label>
	<g:textArea name="description" cols="40" rows="5" maxlength="1000" value="${todoInstance?.description}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: todoInstance, field: 'date', 'error')} required">
	<label for="date">
		<g:message code="todo.date.label" default="Date" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="date" precision="day"  value="${todoInstance?.date}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: todoInstance, field: 'reminderDate', 'error')} ">
	<label for="reminderDate">
		<g:message code="todo.reminderDate.label" default="Reminder Date" />
		
	</label>
	<g:datePicker name="reminderDate" precision="day"  value="${todoInstance?.reminderDate}" default="none" noSelection="['': '']" />

</div>

<div class="fieldcontain ${hasErrors(bean: todoInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="todo.url.label" default="Url" />
		
	</label>
	<g:field type="url" name="url" value="${todoInstance?.url}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: todoInstance, field: 'category', 'error')} ">
	<label for="category">
		<g:message code="todo.category.label" default="Category" />
		
	</label>
	<g:select id="category" name="category.id" from="${es.ua.expertojava.todo.Category.list()}" optionKey="id" value="${todoInstance?.category?.id}" class="many-to-one" noSelection="['null': '']"/>

</div>

<sec:ifAllGranted roles="ROLE_ADMIN">
	<div class="fieldcontain ${hasErrors(bean: todoInstance, field: 'tags', 'error')} ">
		<label for="tags">
			<g:message code="todo.tags.label" default="Tags" />

		</label>
	</div>
</sec:ifAllGranted>

