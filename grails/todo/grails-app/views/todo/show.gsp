
<%@ page import="es.ua.expertojava.todo.Todo" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'todo.label', default: 'Todo')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-todo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<g:render template="topMenu"/>

		<div id="show-todo" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]"/></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list todo">
			
				<g:if test="${todoInstance?.title}">
				<li class="fieldcontain">
					<span id="title-label" class="property-label"><g:message code="todo.title.label" default="Title" /></span>
					
						<span class="property-value" aria-labelledby="title-label"><g:fieldValue bean="${todoInstance}" field="title"/></span>
					
				</li>
				</g:if>
			
				%{--<g:if test="${todoInstance?.realizada}">--}%
				<li class="fieldcontain">
					<span id="realizada-label" class="property-label"><g:message code="todo.realizada.label" default="Realizada" /></span>
					
						<span class="property-value" aria-labelledby="realizada-label"><g:formatBoolean boolean="${todoInstance?.realizada}" /></span>
					
				</li>
				%{--</g:if>--}%
			
				<g:if test="${todoInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="todo.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${todoInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${todoInstance?.date}">
				<li class="fieldcontain">
					<span id="date-label" class="property-label"><g:message code="todo.date.label" default="Date" /></span>
					
						<span class="property-value" aria-labelledby="date-label"><g:formatDate date="${todoInstance?.date}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${todoInstance?.reminderDate}">
				<li class="fieldcontain">
					<span id="reminderDate-label" class="property-label"><g:message code="todo.reminderDate.label" default="Reminder Date" /></span>
					
						<span class="property-value" aria-labelledby="reminderDate-label"><g:formatDate date="${todoInstance?.reminderDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${todoInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="todo.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${todoInstance}" field="url"/></span>
					
				</li>
				</g:if>

				<g:if test="${todoInstance?.category}">
				<li class="fieldcontain">
					<span id="category-label" class="property-label"><g:message code="todo.category.label" default="Category" /></span>
					
						<span class="property-value" aria-labelledby="category-label"><g:link controller="category" action="show" id="${todoInstance?.category?.id}">${todoInstance?.category?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>

				<g:if test="${todoInstance?.dateDone}">
					<li class="fieldcontain">
						<span id="dateDone-label" class="property-label"><g:message code="todo.dateDone.label" default="Date Done" /></span>

						<span class="property-value" aria-labelledby="dateDone-label"><g:formatDate date="${todoInstance?.dateDone}" /></span>

					</li>
				</g:if>
			
				<g:if test="${todoInstance?.tags}">
				<li class="fieldcontain">
					<span id="tags-label" class="property-label"><g:message code="todo.tags.label" default="Tags" /></span>
					
						<g:each in="${todoInstance.tags}" var="t">
						<span class="property-value" aria-labelledby="tags-label"><g:link controller="tag" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:todoInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${todoInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
