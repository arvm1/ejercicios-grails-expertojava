
<%@ page import="es.ua.expertojava.todo.Todo" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <asset:stylesheet src="todo.css"/>
    <g:set var="entityName" value="${message(code: 'todo.label', default: 'Todo')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
    <style type="text/css" media="screen">
    .iconosTodo{
        weight: 25px;
        height: 25px;
    }
    </style>
</head>
<body>
<a href="#list-todo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

<g:render template="topMenu"/>

<div id="list-todo" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table>
        <thead>
        <tr>
            <th>Edition</th>
            <g:sortableColumn property="title" title="${message(code: 'todo.title.label', default: 'Title')}" />

            <g:sortableColumn property="realizada" title="${message(code: 'todo.realizada.label', default: 'Realizada')}" />

            %{--<g:sortableColumn property="description" title="${message(code: 'todo.description.label', default: 'Description')}" />--}%

            <g:sortableColumn property="date" title="${message(code: 'todo.date.label', default: 'Date')}" />

            %{--<g:sortableColumn property="reminderDate" title="${message(code: 'todo.reminderDate.label', default: 'Reminder Date')}" />--}%

            <g:sortableColumn property="url" title="${message(code: 'todo.url.label', default: 'Url')}" />

        </tr>
        </thead>
        <tbody>
        <g:each in="${todoInstanceList}" status="i" var="todoInstance">

            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                <td>
                    <g:form url="[resource:todoInstance, action:'delete']" method="DELETE">
                        <fieldset class="buttons">
                            <g:link class="edit" action="edit" resource="${todoInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                            <g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                        </fieldset>
                    </g:form>
                </td>
                <td><g:link action="show" id="${todoInstance.id}">${fieldValue(bean: todoInstance, field: "title")}</g:link></td>
                <!--Etiqueta que se ha creado en todoTagLibrary que creamos con comando-->
                <td class="iconosTodo"><todo:printIconFromBoolean value="${todoInstance.realizada}"/></td>
                %{--<td><g:formatBoolean boolean="${todoInstance.realizada}" /></td>--}%

                %{--<td>${fieldValue(bean: todoInstance, field: "description")}</td>--}%

                <td><g:formatDate date="${todoInstance.date}" /></td>

                %{--<td><g:formatDate date="${todoInstance.reminderDate}" /></td>--}%

                <td>${fieldValue(bean: todoInstance, field: "url")}</td>

            </tr>
        </g:each>
        </tbody>
    </table>
    <div class="pagination">
        <g:paginate total="${todoInstanceCount ?: 0}" />
    </div>
</div>
</body>
</html>
