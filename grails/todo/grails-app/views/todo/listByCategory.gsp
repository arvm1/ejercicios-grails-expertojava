
<%@ page import="es.ua.expertojava.todo.Todo" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <asset:stylesheet src="todo.css"/>
    <g:set var="entityName" value="${message(code: 'todo.label', default: 'Todo')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
<a href="#list-todo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

<g:render template="topMenu"/>

<div id="list-todo" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:form url="[resource:todoInstance, action:'mostrarTareasByCategorias']" >
        <table>
        <thead>
        <tr>
            <th>Opciones</th>
            <th>Nombre</th>
        </tr>
        </thead>
        <tbody>
            <g:each in="${categoryInstanceList}" status="i" var="categoryInstance">
                <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                    <td><g:checkBox name="categoriaID" value="${categoryInstance?.id}" checked="false"/></td>
                    <td>${categoryInstance?.name}</td>

                </tr>
            </g:each>
        </tbody>
        </table>
        <fieldset class="buttons">
            <g:submitButton name="enviar" class="save" value="${message(code: 'default.button.send.label', default: 'Send')}" />
        </fieldset>
    </g:form>
</div>

</body>
</html>
