<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>Welcome to Grails</title>
		<style type="text/css" media="screen">
			#status {
				background-color: #eee;
				border: .2em solid #fff;
				margin: 2em 2em 1em;
				padding: 1em;
				width: 12em;
				float: left;
				-moz-box-shadow: 0px 0px 1.25em #ccc;
				-webkit-box-shadow: 0px 0px 1.25em #ccc;
				box-shadow: 0px 0px 1.25em #ccc;
				-moz-border-radius: 0.6em;
				-webkit-border-radius: 0.6em;
				border-radius: 0.6em;
			}

			#fotoUsuario{
				width: 180px;
				height: 180px;
				float:left;
			}

			.ie6 #status {
				display: inline; /* float double margin fix http://www.positioniseverything.net/explorer/doubled-margin.html */
			}

			#status ul {
				font-size: 0.9em;
				list-style-type: none;
				margin-bottom: 0.6em;
				padding: 0;
			}

			#status li {
				line-height: 1.3;
			}

			#status h1 {
				text-transform: uppercase;
				font-size: 1.1em;
				margin: 0 0 0.3em;
			}

			#page-body {
				margin: 2em 1em 1.25em 18em;
			}

			h2 {
				margin-top: 1em;
				margin-bottom: 0.3em;
				font-size: 1em;
			}

			p {
				line-height: 1.5;
				margin: 0.25em 0;
			}

			#controller-list ul {
				list-style-position: inside;
			}

			#controller-list li {
				line-height: 1.3;
				list-style-position: inside;
				margin: 0.25em 0;
			}

			@media screen and (max-width: 480px) {
				#status {
					display: none;
				}

				#page-body {
					margin: 0 1em 1em;
				}

				#page-body h1 {
					margin-top: 0;
				}
			}
		</style>
	</head>
	<body>



		<a href="#page-body" class="skip"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div id="status" role="complementary">

		<sec:ifNotLoggedIn>
			<asset:image id="fotoUsuario" src="To-Do-List1.png" alt="fotoUsuario"/>
		</sec:ifNotLoggedIn>
		<sec:ifAllGranted roles="ROLE_ADMIN">
			<asset:image id="fotoUsuario" src="fotoPerfilAdmin.jpeg" alt="fotoUsuario"/>
		</sec:ifAllGranted>
		<sec:ifAllGranted roles="ROLE_BASIC">
			<asset:image id="fotoUsuario" src="fotoAna2.jpg" alt="fotoUsuario"/>
		</sec:ifAllGranted>


		</div>
		<div id="page-body" role="main">
			<h1>Bienvenid@ a la aplicación TODO</h1>
			<p>Nunca antes fue tan fácil gestionar tus tareas!!<br/>
			   Podrás organizarlas en listas e incluso etiquetarlas!! <br/>
			   No esperes más y entra en la aplicación! </p>

			%{--<sec:ifLoggedIn>--}%
				<div id="controller-list" role="navigation">

					%{--<h2>Available Controllers:</h2>--}%
					<ul>
						%{--<g:each var="c" in="${grailsApplication.controllerClasses.sort { it.fullName } }">--}%

						<% miMapaDeControladoresNameAdmin = ["Category":"Categorías","Tag":"Etiquetas","User":"Usuarios"]%>
						<% miMapaDeControladoresNameUsuario = ["Todo":"Tareas"]%>

						<sec:ifAllGranted roles="ROLE_ADMIN">
							<g:each var="c" in="${grailsApplication.domainClasses.sort { it.fullName } }">
								<g:if test="${(miMapaDeControladoresNameAdmin.get(c.name)) != null}">
									<li class="controller">
										<g:link controller="${c.logicalPropertyName}">
											<% def a=miMapaDeControladoresNameAdmin.get(c.name) %>
											Gestionar ${a}
										</g:link>
									</li>
								</g:if>
							</g:each>
						</sec:ifAllGranted>

						<sec:ifAllGranted roles="ROLE_BASIC">
							<g:each var="c" in="${grailsApplication.domainClasses.sort { it.fullName } }">
								<g:if test="${(miMapaDeControladoresNameUsuario.get(c.name)) != null}">
									<li class="controller">
										<g:link controller="${c.logicalPropertyName}" action="showUserTodos">
										Gestionar ${a} <%= miMapaDeControladoresNameUsuario.getAt(c.name) %>
										</g:link>
									</li>
								</g:if>
							</g:each>
						</sec:ifAllGranted>


					</ul>
				</div>
			%{--</sec:ifLoggedIn>--}%
		</div>
	</body>
</html>
