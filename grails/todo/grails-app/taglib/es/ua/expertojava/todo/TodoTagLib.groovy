package es.ua.expertojava.todo

class TodoTagLib {
    static defaultEncodeAs = [taglib:'none']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    static namespace = "todo"
    //Etiqueta creada para mostrar booleanos
    def printIconFromBoolean = { attrs ->
        def value = attrs['value']
        if(value){
            out <<asset.image(src:"todoDone.png")
        }else{
            out <<asset.image(src:"todoNotDone.png")
        }
    }
}
