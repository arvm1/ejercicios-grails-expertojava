package es.ua.expertojava.todo

class Todo {
    String title
    Boolean realizada
    String description
    Date date
    Date reminderDate
    String url
    Category category
    User user
    Date dateDone
    Date dateCreated
    Date lastUpdated

    static hasMany = [tags:Tag]
    /* En una relación muchos a muchos el belongsTo no funciona muy bien,
       así que si quiero borrar una tarea tendré antes que borrar
       las etiquetas que tenga asociadas. */
    static belongsTo = [Tag,User]

    static constraints = {
        title(blank:false)
        realizada(nullable:false)
        description(blank:true, nullable:true, maxSize:1000)
        date(nullable:false)
        url(nullable:true, url:true)
        category(nullable:true)
        user(nullable:true)
        dateDone(nullable:true)

        reminderDate(nullable:true,
                validator: { val, obj ->
                    if (val && obj.date) {
                        return val.before(obj?.date)
                    }
                    return true
                }
        )
    }

    String toString(){
        title
    }
}